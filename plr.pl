#!/usr/bin/env perl

# plr -- Perl REPL
# Copyright (C) 2011  James M. Jensen II <badocelot@badocelot.com>.
#
# This program is free software.  You can redistribute it and/or
# modify it under the terms of the Artistic License 2.0.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# merchantability or fitness for a particular purpose.

use warnings;
use Term::ReadLine;

# initialize plr internals
my %__plr__ = ('version' => 0.23,
	       'version_patch' => 3,
	       'command' => '',
	       '$_ backup' => '');

# Construct header with version number
$__plr__{header} = "Perl REPL v$__plr__{version}";
$__plr__{header} .= ".$__plr__{version_patch}" if ($__plr__{version_patch});

# define some helper functions

# major thanks to Anonymous Monk for this
# from: <http://www.perlmonks.org/?node_id=172613>
sub println {
   local $\ = "\n";
   @_ = ($_) unless @_;
   print @_;
}

sub credits {
   print <<END;
Copyright 2011  James M. Jensen II <badocelot\@badocelot.com>
END
}

sub help {
   my $help = <<END;
Perl REPL lets you test Perl code in an interactive shell.
One caveat: do not use 'my' or 'local' except in blocks.
   Otherwise, the assigned values will not stick.

Defined helper functions (invoke with --pure to undefine):
   credits   -- see who wrote this program
   help      -- display this message (also --help from command line)
   println   -- print with automatic linefeed
END
   print $help;
}

# if help is requested, help shall be given
if (grep $_ eq '--help', @ARGV) {
   print $__plr__{'header'}, "\n";
   credits;
   print "\n";
   help;
   exit;
}

# but if purity is requested, undefine them
# TODO: make this configurable from environment
if (grep $_ eq '--pure', @ARGV) {
   undef *credits;
   undef *help;
   undef *println;
}

# set up the terminal
$__plr__{'term'} = Term::ReadLine->new('Perl REPL');
$__plr__{'term'}->ornaments(0);
$__plr__{'out'} = $__plr__{'term'}->OUT || \*STDOUT;
select $__plr__{'out'};

# initialize brace counts
$__plr__{'open?'} = 0;
for (split //, q|{[("'|) {
   $__plr__{$_} = 0;
}

# print program information
print $__plr__{'header'}, "\n";

# priming read
$__plr__{'next_line'} = $__plr__{'term'}->readline(">> ");

while (defined $__plr__{'next_line'}) {
   # append the line to the command to be executed
   $__plr__{'command'} .= $__plr__{'next_line'} . "\n";
	
   # if there are braces or quotes, stop and count them
   if ($__plr__{'next_line'} =~ /[\{\}\[\]\(\)"']/) {
      my $last_char = '';
      foreach my $char (split //, $__plr__{'next_line'}) {
	 # first check if we're in a string literal
	 if ($__plr__{'"'} || $__plr__{'\''}) {
	    # see if we can close the literal
	    if ($__plr__{'\''} && $char eq '\'' && $last_char ne '\\') {
	       $__plr__{'\''} = 0;
	    } elsif ($__plr__{'"'} && $char eq '"' && $last_char ne '\\') {
	       $__plr__{'"'} = 0;
	    }
	    # otherwise, ignore whatever we find
	 }
	 # if we're not in a string literal, check other things
	 else {
				# do nothing if this is a special variable
	    unless ($last_char eq '$' && $char !~ /[\(\)\{\}]/) {
	       # ignore the rest of the line if it ends with a comment
	       last if ($char eq '#');
					
	       # check for opening braces
	       if ($char =~ /[\{\[\(]/) {
		  ++$__plr__{$char};
	       }
	       # then closing braces
	       elsif ($char =~ /[\}\]\)]/) {
		  # match with opening brace to update correct count
		  my %open_brace = (
				    '}' => '{',
				    ']' => '[',
				    ')' => '(');
		  --$__plr__{$open_brace{$char}};
	       }
	       # then try to open a string literal
	       elsif ($char eq '"' or $char eq "'") {
		  $__plr__{$char} = 1;
	       }
	    }
	 }
			
	 # copy $char --> $last_char
	 $last_char = $char
      }
   }
	
   # check if anything is open
   $__plr__{'open?'} = 0;
   for (split //, q|{[("'|) {
      if ($__plr__{$_} != 0) {
	 $__plr__{'open?'} = 1;
      }
   }
	
   # uncomment the following for debugging
   #for (keys %__plr__) {
   #	print "$_ => $__plr__{$_}\n" if (defined $__plr__{$_})
   #}
	
   # if not, evaluate the command
   if (!$__plr__{'open?'}) {
      # restore $_ from backup
      $_ = $__plr__{'$_ backup'};
		
      # evaluate command
      $__plr__{'val'} = eval($__plr__{'command'});
		
      # backup new $_
      $__plr__{'$_ backup'} = $_;
		
      # print warning, if any, and the evaluated result
      warn $@ if $@;
      if (defined $__plr__{'val'}) {
	 print "=> ", $__plr__{'val'}, "\n";
      }
		
      # clear the stored command
      $__plr__{'command'} = '';
   }
	
   # next input
   $__plr__{'next_line'} =
     $__plr__{'term'}->readline(($__plr__{'open?'}) ? ".. " : ">> ");
}
